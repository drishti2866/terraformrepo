# ------------------------------------------------------------------------------
# adoprivatednszone Authentication
# ------------------------------------------------------------------------------
variable "subscription_id" {
  description = "Azure subscription Id."
  type        = string
}

variable "tenant_id" {
  description = "Azure tenant Id."
  type        = string
}

variable "client_id" {
  description = "Azure service principal application Id."
  type        = string
  default     = null
}

variable "client_secret" {
  description = "Azure service principal application Secret."
  type        = string
  default     = null
}

variable "ado_subscription_id" {
  description = "Id of the Azure subscription that ADO resources are located in."
  type        = string
  default     = "[__ado_subscription_id__]"
}

# ------------------------------------------------------------------------------
# adoprivatednszone Providers
# ------------------------------------------------------------------------------
terraform {
  required_version = ">= 1.0.0"
  backend "azurerm" {}
}

terraform {
  required_providers {
    azurerm = {
      version = "= 3.23.0"
    }
    azuread = {
      version = "= 2.36.0"
    }
  }
}

provider "azurerm" {
  tenant_id        = var.tenant_id
  subscription_id  = var.subscription_id
  client_id        = var.client_id
  client_secret    = var.client_secret
  partner_id       = "a79fe048-6869-45ac-8683-7fd2446fc73c"
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
}

provider "azurerm" {
  alias            = "ado"
  tenant_id        = var.tenant_id
  subscription_id  = var.ado_subscription_id
  client_id        = var.client_id
  client_secret    = var.client_secret
  partner_id       = "a79fe048-6869-45ac-8683-7fd2446fc73c"
  features {
    key_vault {
      purge_soft_delete_on_destroy = true
    }
  }
}


# ------------------------------------------------------------------------------
# Begin adoprivatednszone module input variable definitions
# ------------------------------------------------------------------------------
variable "resource_group_name" {
  description = "The name of the 'primary' resource group where the DNS zones will be created."
  type        = string
}

variable "tags" {
  type        = map(string)
  description = <<EOT
Additional tags, beyond those on the 'primary' resource group,
that will be added to any resources that are created by this module.
EOT
  default     = {}
}

variable "private_dns_zones" {
  type = map(object({
    dns_zone_name = string
  }))
  description = <<EOT
Map containing details about private DNS zones to create in the
'primary' resource group.
EOT
  default     = {}
}

variable "private_dns_zone_links" {
  type = map(object({ 
    link_name            = string
    dns_zone_key         = string
    dns_zone_name        = string
    vnet_id              = string
    registration_enabled = bool
  }))
  description = <<EOT
Map containing details about links to establish between private DNS
zones and virtual networks in the 'primary' resource group.
EOT
  default     = {}
}

variable "ado_resource_group_name" {
  description = <<EOT
The name of the 'ado' resource group where the DNS zones that
support your ADO resources will be created.
EOT
  type        = string
  default     = null
}

variable "ado_tags" {
  type        = map(string)
  description = <<EOT
Additional tags, beyond those on the 'ado' resource group, that
will be added to any resources that are created by this module.
EOT
  default     = {}
}

variable "ado_private_dns_zones" {
  type = map(object({
    dns_zone_name = string
  }))
  description = <<EOT
Map containing details about private DNS zones to create in
the 'ado' resource group.
EOT
  default     = {}
}

variable "ado_private_dns_zone_links" {
  type = map(object({ 
    link_name            = string
    dns_zone_key         = string
    dns_zone_name        = string
    vnet_id              = string
    registration_enabled = bool
  }))
  description = <<EOT
Map containing details about links to establish between private
DNS zones and virtual networks in the 'ado' resource group.
EOT
  default     = {}
}
# ------------------------------------------------------------------------------
# End adoprivatednszone module input variable definitions
# ------------------------------------------------------------------------------

# ------------------------------------------------------------------------------
# Begin adoprivatednszone module invocation
# ------------------------------------------------------------------------------
module "privatednszone_adoprivatednszone" {
  source                     = "terraform.it.att.com/pcmodules/privatednszone/azurerm"
  version                    = "2.0.0"

  providers                  = {
    azurerm                  = azurerm
    azurerm.ado              = azurerm.ado
  }

  resource_group_name        = var.resource_group_name
  tags                       = var.tags
  private_dns_zones          = var.private_dns_zones
  private_dns_zone_links     = var.private_dns_zone_links
  ado_resource_group_name    = var.ado_resource_group_name
  ado_tags                   = var.ado_tags
  ado_private_dns_zones      = var.ado_private_dns_zones
  ado_private_dns_zone_links = var.ado_private_dns_zone_links
}
# ------------------------------------------------------------------------------
# End adoprivatednszone module invocation
# ------------------------------------------------------------------------------

